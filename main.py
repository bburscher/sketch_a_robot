import turtle as t
import time

def rectangle(hor, vert, color, clockwise=True):
    t.pendown()
    t.pensize(1)
    t.shape('turtle')
    t.color(color)
    t.begin_fill()
    for counter in range(1, 3):
        t.forward(hor)
        t.right(90) if clockwise else t.left(90)
        t.forward(vert)
        t.right(90) if clockwise else t.left(90)
    t.end_fill()
    t.penup()
    t.speed('fast')
    t.bgcolor('Dodger Blue')

def make_feet():
    t.goto(-100, -150)
    rectangle(50, 20, 'lawn green')
    t.goto(-30, -150)
    rectangle(50, 20, 'lawn green')

def make_legs():
    t.goto(-25, -50)
    rectangle(15, 100, 'light blue')
    t.goto(-55, -50)
    rectangle(-15, 100, 'light blue')

def make_body():
    t.goto(-90, 100)
    rectangle(100, 150, 'hot pink')

def make_heart(x=0, y=0, size=40):
    t.color('red')
    t.goto(x-3./4*size, y)
    t.begin_fill()
    t.circle(size)
    t.end_fill()
    t.goto(x+3./4*size, y)
    t.begin_fill()
    t.circle(size)
    t.end_fill()
    t.goto(x, y-size*9/8)
    t.begin_fill()
    t.right(180+45)
    rectangle(size, 2*size, 'red')
    t.end_fill()
    t.goto(x, y-size*9/8)
    t.begin_fill()
    t.right(90)
    rectangle(size, 2*size, 'red', clockwise=False)
    t.end_fill()
    t.right(45) # reset

def make_arms():
    t.goto(-150, 70)
    rectangle(60, 15, 'light blue')
    t.goto(-150, 110)
    rectangle(15, 40, 'light blue')

    t.goto(10, 70)
    rectangle(60, 15, 'light blue')
    t.goto(55, 110)
    rectangle(15, 40, 'light blue')

def make_neck():
    t.goto(-50, 120)
    rectangle(15, 20, 'light blue')

def make_head():
    t.goto(-85, 170)
    rectangle(80, 50, 'goldenrod')

def make_eyes():
    t.goto(-60, 160)
    rectangle(30, 10, 'white')
    t.goto(-55, 155)
    rectangle(5, 5, 'black')
    t.goto(-40, 155)
    rectangle(5, 5, 'black')

def make_eyes_funny():
    t.goto(-60, 160)
    rectangle(30, 10, 'white')
    t.goto(-60, 160)
    rectangle(5, 5, 'black')
    t.goto(-45, 155)
    rectangle(5, 5, 'black')

def make_mouth():
    t.goto(-65, 135)
    t.right(5)
    rectangle(40, 5, 'black')

def make_hands():
    t.goto(-155, 130)
    rectangle(25, 25, 'lawn green')
    t.goto(-147, 130)
    rectangle(10, 15, t.bgcolor())
    t.goto(50, 130)
    rectangle(25, 25, 'lawn green')
    t.goto(58, 130)
    rectangle(10, 15, t.bgcolor())

def make_robot():
    make_feet()
    make_legs()
    make_body()
    make_heart(x=-40, y=20, size=20)
    make_arms()
    make_hands()
    make_neck()
    make_head()
    make_mouth()
    make_eyes()


if __name__ == '__main__':
    t.penup()
    make_robot()
    t.hideturtle()
    time.sleep(10)

